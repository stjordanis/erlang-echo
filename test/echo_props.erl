%%%-------------------------------------------------------------------
%%% @author Laura Castro <lcastro@udc.es>
%%% @copyright (C) 2019, Laura Castro
%%% @doc An echo server (test module)
%%% @end
%%%-------------------------------------------------------------------
-module(echo_props).

-include_lib("proper/include/proper.hrl").

%% PropER callbacks
-export([initial_state/0, command/1, precondition/2, next_state/3, postcondition/3]).
%% Model wrappers
-export([start/0, echo/1, stop/0]).

-define(SUT_MODULE, echo).

%% Initialize the state
initial_state() ->
    stopped.

%% Command generator, S is the state
command(_S) ->
    frequency([{30, {call, ?MODULE, start, []}},
               {60, {call, ?MODULE, echo, [message()]}},
               {10, {call, ?MODULE, stop, []}}]).

message() ->
    string().

%% Next state transformation, S is the current state
next_state(_S, _V, {call, ?MODULE, start, []}) -> started;
next_state(_S, _V, {call, ?MODULE, stop,  []}) -> stopped;
next_state(S,  _V, {call, _, _, _}) -> S.

%% Precondition, checked before command is added to the command sequence
precondition(_S, {call, _, _, _}) -> true.

%% Postcondition, checked after command has been evaluated
%% OBS: S is the state before next_state(S,_,<command>)
postcondition(started, {call, _, start, _}, Res)  -> Res == already_started;
postcondition(stopped, {call, _, start, _}, Res)  -> Res == ok;
postcondition(started, {call, _, echo, [M]}, Res) -> Res == M;
postcondition(stopped, {call, _, echo, _}, Res)   -> Res == server_not_started;
postcondition(started, {call, _, stop, _}, Res)   -> Res == ok;
postcondition(stopped, {call, _, stop, _}, Res)   -> Res == already_stopped;
postcondition(_S, {call, _, _, _}, _Res) -> false.

%% TEST PROPERTY
prop_echoserver() ->
    ?FORALL(Cmds, commands(?MODULE),
            begin
                startup(),
                {History, State, Res} = run_commands(?MODULE, Cmds),
                cleanup(State),
                ?WHENFAIL(io:format("H: ~p~nS: ~p~n Res: ~p~n",
                                    [History, State, Res]),
                          Res==ok)
            end).

%% Test function wrappers
start() ->
    try ?SUT_MODULE:start() of
        ok -> ok
    catch
        error:badarg ->
            already_started
    end.

echo(Message) ->
    try ?SUT_MODULE:echo(Message) of
        Message -> Message
    catch
        error:badarg ->
            server_not_started
    end.

stop() ->
    try ?SUT_MODULE:stop() of
        ok -> ok
    catch
        error:badarg ->
            already_stopped
    end.

%% Private utilitary stuff
startup() ->
    ok.

cleanup(started) ->
    ok = ?SUT_MODULE:stop();
cleanup(stopped) ->
    ok.
