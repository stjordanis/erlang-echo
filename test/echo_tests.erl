%%%-------------------------------------------------------------------
%%% @author Laura Castro <lcastro@udc.es>
%%% @copyright (C) 2019, Laura Castro
%%% @doc An echo server (traditional test module)
%%% @end
%%%-------------------------------------------------------------------
-module(echo_tests).

-include_lib("eunit/include/eunit.hrl").

echo_test() ->
    ok = echo:start(),
    Message = "Lambda World Cádiz",
    ?assertEqual(Message, echo:echo(Message)),
    ok = echo:stop().

echo_test_() ->
    {setup,
     fun()  -> ok = echo:start() end, % (1) test setup
     fun(_) -> ok = echo:stop()  end, % (2) test cleanup
     fun(_) ->
             Message = "Lambda World Cádiz",
             {inorder,
              [?_assertEqual( Message,  echo:echo(Message) ),
               ?_assertEqual( Message,  echo:echo(Message) ),
               ?_assertEqual( Message,  echo:echo(Message) ),
               ?_assertEqual( Message,  echo:echo(Message) ),
               ?_assertEqual( Message,  echo:echo(Message) ),
               ?_assertEqual( Message,  echo:echo(Message) )]}
     end}.

unexpected_echo_test() ->
    ok = echo:start(),
    Message = "Lambda World Cádiz",
    P = spawn(fun() -> agent(echo, [Message], self()) end),
    {ok, _TRef} = timer:kill_after(1, P),
    receive
    after 2 ->
            ?assert(true)
    end,
    ok = echo:stop().

agent(Process, Message, Caller) ->
    Process ! Message,
    Result = receive
                 Something -> Something
             end,
    Caller ! {self(), Result}.

autostop_idle_echo_test() ->
    ok = echo:start(),
    receive
    after 2010 ->
            ?assertEqual(undefined, whereis(echo))
    end.
