%%%-------------------------------------------------------------------
%%% @author Laura Castro <lcastro@udc.es>
%%% @copyright (C) 2019, Laura Castro
%%% @doc An echo server (supervisable).
%%%      This implementation differs from the `echo' module in that
%%%      it uses the `gen_server' behaviour.
%%% @end
%%%-------------------------------------------------------------------
-module(gen_echo).
-behaviour(gen_server).

%% PUBLIC API
-export([echo/1]).

%% SUPERVISOR API
-export([start/0]).

%% gen_server callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         terminate/2, code_change/3, format_status/2]).

-define(ECHO_SERVER, echo).

%% @doc Starts the echo server
-spec start() -> {ok, Pid :: pid()} |
                 {error, Error :: {already_started, pid()}} |
                 {error, Error :: term()} |
                 ignore.
start() ->
    gen_server:start_link({local, ?ECHO_SERVER}, ?MODULE, [], []).

%% @doc Sends a message `Message' to the echo server and returns its reply
-spec echo(Message :: any()) -> any().
echo(Message) ->
    gen_server:call(?ECHO_SERVER, {echo, Message}).

% Internal
% @private
% @doc Initializes the server
-spec init(Args :: term()) -> {ok, State :: term()} |
                              {ok, State :: term(), Timeout :: timeout()} |
                              {ok, State :: term(), hibernate} |
                              {stop, Reason :: term()} |
                              ignore.
init([]) ->
    % process_flag(trap_exit, true), % we do want the process to die
    {ok, []}.

% @private
% @doc Handling call messages
-spec handle_call(Request :: term(), From :: {pid(), term()}, State :: term()) ->
                         {reply, Reply :: term(), NewState :: term()} |
                         {reply, Reply :: term(), NewState :: term(), Timeout :: timeout()} |
                         {reply, Reply :: term(), NewState :: term(), hibernate} |
                         {noreply, NewState :: term()} |
                         {noreply, NewState :: term(), Timeout :: timeout()} |
                         {noreply, NewState :: term(), hibernate} |
                         {stop, Reason :: term(), Reply :: term(), NewState :: term()} |
                         {stop, Reason :: term(), NewState :: term()}.
handle_call({echo, Message}, _From, State) ->
    Reply = Message,
    {reply, Reply, State}.

% @private
% @doc Handling cast messages
-spec handle_cast(Request :: term(), State :: term()) ->
                         {noreply, NewState :: term()} |
                         {noreply, NewState :: term(), Timeout :: timeout()} |
                         {noreply, NewState :: term(), hibernate} |
                         {stop, Reason :: term(), NewState :: term()}.
handle_cast(_Request, State) ->
    {noreply, State}.

% @private
% @doc Handling all non call/cast messages
-spec handle_info(Info :: timeout() | term(), State :: term()) ->
                         {noreply, NewState :: term()} |
                         {noreply, NewState :: term(), Timeout :: timeout()} |
                         {noreply, NewState :: term(), hibernate} |
                         {stop, Reason :: normal | term(), NewState :: term()}.
handle_info(_Info, State) ->
    {noreply, State}.

% @private
% @doc
% This function is called by a gen_server when it is about to
% terminate. It should be the opposite of Module:init/1 and do any
% necessary cleaning up. When it returns, the gen_server terminates
% with Reason. The return value is ignored.
% @end
-spec terminate(Reason :: normal | shutdown | {shutdown, term()} | term(),
                State :: term()) -> any().
terminate(_Reason, _State) ->
    ok.

% @private
% @doc Convert process state when code is changed
-spec code_change(OldVsn :: term() | {down, term()},
                  State :: term(),
                  Extra :: term()) -> {ok, NewState :: term()} |
                                      {error, Reason :: term()}.
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

% @private
% @doc
% This function is called for changing the form and appearance
% of gen_server status when it is returned from sys:get_status/1,2
% or when it appears in termination error logs.
% @end
-spec format_status(Opt :: normal | terminate,
                    Status :: list()) -> Status :: term().
format_status(_Opt, Status) ->
    Status.
