%%%-------------------------------------------------------------------
%%% @author Laura Castro <lcastro@udc.es>
%%% @copyright (C) 2019, Laura Castro
%%% @doc A header file for distribution values configuration.
%%% @end
%%%-------------------------------------------------------------------

-define(NODES, [{alice,localhost}, {bob,localhost}, {carol,localhost}, {dan, localhost}]).
-define(DOMAIN, "@lynceo.localdomain").
-define(TEST_COOKIE, "testcookie").
-define(PATH, "_build/testeqc/lib/erlang_echo/ebin/").
